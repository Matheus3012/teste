﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Nsf._2018.Modulo3.App.DB.Produto;
using Nsf._2018.Modulo3.App.DB.Pedido;

namespace Nsf._2018.Modulo3.App.Telas
{
    public partial class frmPedidoCadastrar : UserControl
    {
        BindingList<ProdutoDTO> produtosCarrinho = new BindingList<ProdutoDTO>();


        public frmPedidoCadastrar()
        {
            InitializeComponent();
            CarregarCombos();
            ConfigurarGrid();
        }


        void CarregarCombos()
        {
            ProdutoBusiness business = new ProdutoBusiness();
            List<ProdutoDTO> lista = business.Listar();

            cboProduto.ValueMember = nameof(ProdutoDTO.Id);
            cboProduto.DisplayMember = nameof(ProdutoDTO.Nome);
            cboProduto.DataSource = lista;




        }
        void ConfigurarGrid()
        {
            dgvItens.AutoGenerateColumns = false;
            dgvItens.DataSource = produtosCarrinho;


        }
        private void dgvItens_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 2)
            {
                ProdutoDTO produto = dgvItens.CurrentRow.DataBoundItem as ProdutoDTO;

                DialogResult r = MessageBox.Show("Deseja excluir o item selecionado?", "Nsf DBZ",
                                    MessageBoxButtons.YesNo,
                                    MessageBoxIcon.Question);

                if (r == DialogResult.Yes)
                {
                    ProdutoBusiness business = new ProdutoBusiness();
                    business.Remover(produto.Id);

                    ConfigurarGrid();
                }




            }
        }
        private void btnEmitir_Click(object sender, EventArgs e)
        {
            PedidoDTO dto = new PedidoDTO();
            dto.Cliente = txtCliente.Text;
            dto.Cpf = txtCpf.Text;
            dto.Data = DateTime.Now;

            PedidoBusiness business = new PedidoBusiness();
            business.Salvar(dto, produtosCarrinho.ToList());

            MessageBox.Show("Pedido salvo com sucesso.", "DBZ", MessageBoxButtons.OK, MessageBoxIcon.Information);
            this.Hide();



        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            ProdutoDTO dto = cboProduto.SelectedItem as ProdutoDTO;

            int qtd = Convert.ToInt32(txtQuantidade.Text);

            for (int i = 0; i < qtd; i++)
            {
                produtosCarrinho.Add(dto);
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            ProdutoDTO dto = cboProduto.SelectedItem as ProdutoDTO;

            int qtd = Convert.ToInt32(txtQuantidade.Text);

            for (int i =0; i < qtd; i++)
            {
                produtosCarrinho.Remove(dto);
            }

        }

        private void dgvItens_CellContentClick_1(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
