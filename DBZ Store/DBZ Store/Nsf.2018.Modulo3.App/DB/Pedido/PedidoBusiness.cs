﻿using Nsf._2018.Modulo3.App.DB.Pedido_Item;
using Nsf._2018.Modulo3.App.DB.Produto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.Modulo3.App.DB.Pedido
{
    class PedidoBusiness
    {
        public int Salvar(PedidoDTO pedido, List<ProdutoDTO> produto)
        {
            PedidoDataBase pedidoDatabase = new PedidoDataBase();

            int idPedidos = pedidoDatabase.Salvar(pedido);

            PedidoItemBusiness itemBusiness = new PedidoItemBusiness();

            foreach (ProdutoDTO item in produto)
            {
                PedidoItemDTO itemDto = new PedidoItemDTO();
                itemDto.IdPedido = idPedidos;
                itemDto.IdProduto = item.Id;

                itemBusiness.Salvar(itemDto);
            }

            return idPedidos;
        }

        public List<PedidoConsultarView> Consultar(string nomecliente)
        {
            PedidoDataBase pedidoDatabase = new PedidoDataBase();
            return pedidoDatabase.Consultar(nomecliente);
        }

    }
}
