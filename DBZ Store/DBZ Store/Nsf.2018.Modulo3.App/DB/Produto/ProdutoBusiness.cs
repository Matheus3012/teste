﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.Modulo3.App.DB.Produto
{
    class ProdutoBusiness
    {
        public int Salvar(ProdutoDTO dto)
        {
            ProdutoDataBase db = new ProdutoDataBase();
            return db.Salvar(dto);
        }

       

        public List<ProdutoDTO> Consultar(string produto)
        {
            ProdutoDataBase db = new ProdutoDataBase();
            return db.Consultar(produto);
        }

        public List<ProdutoDTO> Listar()
        {
            ProdutoDataBase db = new ProdutoDataBase();
            return db.Listar();
        }
        public void Remover(int id)
        {
            ProdutoDataBase db = new ProdutoDataBase();
            db.Remover(id);
        }

    }
}
