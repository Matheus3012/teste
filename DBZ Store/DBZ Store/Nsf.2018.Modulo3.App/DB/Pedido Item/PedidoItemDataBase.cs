﻿using MySql.Data.MySqlClient;
using Nsf._2018.Modulo3.App.DB.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.Modulo3.App.DB.Pedido_Item
{
    class PedidoItemDataBase
    {
        public int Salvar(PedidoItemDTO pedidoitem)
        {
            string script = @"INSERT INTO tb_pedido_item(id_pedido,id_produto)
                              VALUES(@id_pedido,@id_produto)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_produto", pedidoitem.IdProduto));
            parms.Add(new MySqlParameter("id_pedido", pedidoitem.IdPedido));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);


        }






    }

}
